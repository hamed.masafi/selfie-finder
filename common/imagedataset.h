#ifndef IMAGEDATASET_H
#define IMAGEDATASET_H

#include <SharedObject>
#include <QImage>

class ImageDataset : public Neuron::SharedObject
{
    Q_OBJECT

    N_CLASS_DECL(ImageDataset)

    N_PROPERTY_DECL(QImage, image, image, setImage, imageChanged)
    N_REMOTE_METHOD_DECL(QVariantList, getImages)
};

#endif // IMAGEDATASET_H
