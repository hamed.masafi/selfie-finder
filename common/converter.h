#ifndef CONVERTER_H
#define CONVERTER_H

#include <QVariantMap>
#include <functional>

class Converter
{
public:
    Converter();

    template<typename T>
    static void convert(const QVariantMap &map, QMap<QString, T> &out);
    template<typename T>
    static void convert(const QMap<QString, T> &out, const QVariantMap &map);
    template<typename T>
    static void convert(const QVariantMap &map, QList<T> list, std::function<T(QVariantMap::ConstIterator)> &callback);

    template<typename T>
    static void convert(const QVariantList &list, QList<T> &out);
    template<typename T>
    static void convert(const QList<T> &out, const QVariantList &list);
    template<typename T>
    static void convert(const QVariantList &list, QList<T> out, std::function<T(QVariant)> &callback);
};

template<typename T>
void Converter::convert(const QVariantList &list, QList<T> out, std::function<T(QVariant)> &callback)
{
    auto i = list.begin();
    out.append(callback(*i));
}

template<typename T>
void Converter::convert(const QVariantMap &map, QList<T> list, std::function<T (QVariantMap::ConstIterator)> &callback)
{
    auto i = map.begin();
    while (i != map.end()) {
        list.append(callback(i));
        ++i;
    }
}

template<typename T>
void Converter::convert(const QList<T> &list, const QVariantList &out)
{
    auto i = list.begin();
    while (i != list.end()) {
        out.append(*i);
        ++i;
    }
}

template<typename T>
void Converter::convert(const QMap<QString, T> &map, const QVariantMap &out)
{
    auto i = map.begin();
    while (i != map.end()) {
        out.insert(i.key(), i.value());
        ++i;
    }
}

template<typename T>
void Converter::convert(const QVariantList &list, QList<T> &out)
{
    auto i = list.begin();
    while (i != list.end()) {
        out.append((*i).value<T>());
        ++i;
    }
}

template<typename T>
void Converter::convert(const QVariantMap &map, QMap<QString, T> &out)
{
    auto i = map.begin();
    while (i != map.end()) {
        out.insert(i.key(), i.value().value<T>());
        ++i;
    }
}

#endif // CONVERTER_H
