#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <Peer>
#include <QImage>

#define NEURON_VALIDATE_TOKEN       "saj2l3kj09vEWjh&*@89d9Jkhksad="
#define PORT            8099

class Processor : public Neuron::Peer
{
    Q_OBJECT

    typedef QMap<int, int> Result;
#ifdef NEURON_CLIENT
    QImage image;
#endif

    N_CLASS_DECL(Processor)
    N_REMOTE_METHOD_DECL(finished, int, id, int, psnr)

    N_REMOTE_METHOD_DECL(showImage, QString, fileName, int, time)
    N_REMOTE_METHOD_DECL(showMessage, QString, message)
    N_REMOTE_METHOD_DECL(search, QImage, image)
    N_REMOTE_METHOD_DECL(setList, QVariantList, list)

    N_PROPERTY_DECL(int, workersCount, workersCount, setWorkersCount, workersCountChanged)
    N_PROPERTY_DECL(int, progressValue, progressValue, setProgressValue, progressValueChanged)
    N_PROPERTY_DECL(int, progressMax, progressMax, setProgressMax, progressMaxChanged)
    N_PROPERTY_DECL(bool, isBusy, isBusy, setIsBusy, isBusyChanged)

    signals:
        void newImage(const QImage &image);
        void imageFound(const QString &fileName, const int &time);
        void resultReturned(int id, int psnr);
        void messageRecived(const QString &message);
};

Q_DECLARE_METATYPE(Processor*)

#endif // PROCESSOR_H
