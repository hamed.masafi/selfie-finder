 INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/consts.h \
    $$PWD/converter.h \
    $$PWD/imagedataset.h \
    $$PWD/processor.h

SOURCES += \
    $$PWD/converter.cpp \
    $$PWD/imagedataset.cpp \
    $$PWD/processor.cpp
