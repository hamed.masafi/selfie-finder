#ifndef CONSTS_H
#define CONSTS_H

#define IMAGES_PATH             "/home/hamed/Downloads/Selfie-dataset/images/"
#define IMAGE_NAME_TEMPLATE     "image_%1.jpg"
#define IMAGES_COUNT            46836
#define DATASET_PATH            IMAGES_PATH IMAGE_NAME_TEMPLATE

#endif // CONSTS_H
