#include "imagematcher.h"
#include "imagematcherpool.h"
#include <opencv2/opencv.hpp>
#include <QDebug>
#include <QFile>
#include "../common/consts.h"

using namespace cv;

ImageMatcher::ImageMatcher(ImageMatcherPool *parent, Mat baseMat, int index) :
    QRunnable (), _parent(parent), _baseMat(baseMat), _index(index)
{
    setAutoDelete(true);
}

void ImageMatcher::run()
{
    auto fileName = QString(DATASET_PATH).arg(_index);
    if (!QFile::exists(fileName)) {
        qWarning("File does not exists: %s", qPrintable(fileName));
        return;
    }
    Mat matchMat = imread(fileName.toLocal8Bit().data());
    Mat s1;
    absdiff(_baseMat, matchMat, s1);       // |I1 - I2|

    cvtColor(s1, s1, CV_BGR2GRAY);
    int ret = cv::countNonZero(s1);
//    imwrite(QString("/home/hamed/dev/Qt/build/ImageDist/Desktop_Qt_5_13_0_GCC_64bit4/Debug/test/%1.jpg").arg(_index).toLocal8Bit().data(),
//            s1);

    _parent->worker_finished(_index, ret);
}
