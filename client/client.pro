#-------------------------------------------------
#
# Project created by QtCreator 2019-08-11T18:54:47
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        imagematcher.cpp \
        imagematcherpool.cpp \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        imagematcher.h \
        imagematcherpool.h \
        mainwindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include(../common/common.pri)
include(../../Neuron/neuron_client.pri)

win32 {
    INCLUDEPATH += D:/dev/libs/opencv-dist/include

    LIBS += -LD:/dev/libs/opencv-dist/lib \
            -lopencv_dnn340d        \
            -lopencv_imgcodecs340d   \
            -lopencv_imgcodecs340d \
            -lopencv_features2d340d
} else {
    LIBS += -lopencv_core \
        -lopencv_dnn \
        -lopencv_imgproc \
        -lopencv_imgcodecs \
        -lopencv_features2d
}


