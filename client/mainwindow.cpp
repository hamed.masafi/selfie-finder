#include "mainwindow.h"

#include "processor.h"
#include "../common/consts.h"

#include <ClientHub>
#include <SimpleTokenValidator>

#include <QFileDialog>

using namespace Neuron;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    connectToServer();
}

void MainWindow::connectToServer()
{
    Neuron::ClientHub *hub = new ClientHub(this);
    hub->setObjectName("hub");
    hub->setEncoder(new Neuron::SimpleTokenValidator(NEURON_VALIDATE_TOKEN));
    hub->connectToHost("127.0.0.1", PORT);

    connect(hub, &AbstractHub::statusChanged, [this](AbstractHub::Status status){
        switch (status) {
        case AbstractHub::Connected:
            labelStatus->setText("Connected");
            break;
        case AbstractHub::Reconnecting:
            labelStatus->setText("Reconnecting");
            break;
        case AbstractHub::Unconnected:
            labelStatus->setText("Unconnected");
            break;
        }
    });
    _processor = new Processor(hub, this);
    _processor->setObjectName("processor");

    connect(_processor, &Processor::workersCountChanged, [this](int workersCount){
        labelStatusWorkersCount->setText(QString::number(workersCount));
    });
    connect(_processor, &Processor::newImage, [this](const QImage &image){
        labelImage->setPixmap(QPixmap::fromImage(image));
    });
    connect(_processor, &Processor::imageFound, [this](const QString &fileName, const int &time){
        labelFoundImage->setPixmap(QPixmap(IMAGES_PATH + fileName));
        labelElapsed->setText(QString("%1 ms").arg(time));
    });
    connect(_processor, &Processor::messageRecived, labelElapsed, &QLabel::setText);
    connect(_processor, &Processor::progressValueChanged, progressBar, &QProgressBar::setValue);
    connect(_processor, &Processor::progressMaxChanged, progressBar, &QProgressBar::setMaximum);
    connect(_processor, &Processor::isBusyChanged, pushButtonSelectImage, &QAbstractButton::setDisabled);
}

void MainWindow::on_pushButtonSelectImage_clicked()
{
    QFileDialog d;
    d.setNameFilter("Image files (*.bmp *.jpg *.jpeg *.png);; All files (*)");
//    d.setDirectory(IMAGES_PATH);
    QString fileName = d.getOpenFileName(this);
    if (!fileName.isEmpty()) {
        QImage img(fileName);
        if (!img.isNull())
            _processor->search(img);
    }
}
