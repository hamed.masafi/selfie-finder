#include "imagematcher.h"
#include "imagematcherpool.h"
#include <opencv2/opencv.hpp>
#include <QDebug>

using namespace cv;

ImageMatcherPool::ImageMatcherPool() : QThreadPool(), m_bestId(0), m_bestPsnr(-1)
{
    //connect(this, &QThreadPool::activeThreadCount)
}

void ImageMatcherPool::imageToMat(const QImage &image, cv::OutputArray out)
{
    switch(image.format()) {
        case QImage::Format_Invalid:
        {
        Q_UNREACHABLE();
            Mat empty;
            empty.copyTo(out);
            break;
        }
        case QImage::Format_RGB32:
        {
            Mat view(image.height(),image.width(),
                     CV_8UC4,
                     (void *)image.constBits(),
                     image.bytesPerLine());
            cvtColor(view, out, COLOR_RGBA2RGB);
            break;
        }
        case QImage::Format_RGB888:
        {
        Q_UNREACHABLE();
            Mat view(image.height(),image.width(),CV_8UC3,(void *)image.constBits(),image.bytesPerLine());
            cvtColor(view, out, COLOR_RGB2BGR);
            break;
        }
        default:
        {
        Q_UNREACHABLE();
            QImage conv = image.convertToFormat(QImage::Format_ARGB32);
            Mat view(conv.height(),conv.width(),CV_8UC4,(void *)conv.constBits(),conv.bytesPerLine());
            view.copyTo(out);
            break;
        }
    }

}

void ImageMatcherPool::run(const int &id)
{
    auto m = new ImageMatcher(this, mat.clone(), id);
//    connect(m, &ImageMatcher::finished, this, &ImageMatcherPool::worker_finished);
    start(m);
}

QImage ImageMatcherPool::image() const
{
    return m_image;
}

int ImageMatcherPool::bestId() const
{
    return m_bestId;
}

int ImageMatcherPool::bestPsnr() const
{
    return m_bestPsnr;
}

int ImageMatcherPool::count() const
{
    return m_count;
}

void ImageMatcherPool::worker_finished(int id, int result)
{
    if (result < m_bestPsnr || m_bestPsnr == -1) {
        m_bestPsnr = result;
        m_bestId = id;
    }

    if (!--m_remains)
        emit finished(m_bestId, m_bestPsnr);
}

void ImageMatcherPool::setImage(QImage image)
{
    if (m_image == image)
        return;

    m_image = image;
    emit imageChanged(m_image);
    imageToMat(image, mat);
//    imwrite("/home/hamed/dev/Qt/build/ImageDist/Desktop_Qt_5_13_0_GCC_64bit4/Debug/out.png", mat);
}

void ImageMatcherPool::setBestId(int bestId)
{
    if (m_bestId == bestId)
        return;

    m_bestId = bestId;
    emit bestIdChanged(m_bestId);
}

void ImageMatcherPool::setBestPsnr(int bestPsnr)
{
    if (m_bestPsnr == bestPsnr)
        return;

    m_bestPsnr = bestPsnr;
    emit bestPsnrChanged(m_bestPsnr);
}

void ImageMatcherPool::setCount(int count)
{
    if (m_count == count)
        return;

    m_remains = m_count = count;
    emit countChanged(m_count);
}
