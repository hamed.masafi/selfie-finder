#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"

class Processor;
class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT
    Processor *_processor;


    void connectToServer();
public:
    explicit MainWindow(QWidget *parent = nullptr);
private slots:
    void on_pushButtonSelectImage_clicked();
};

#endif // MAINWINDOW_H
