#ifndef IMAGEMATCHERPOOL_H
#define IMAGEMATCHERPOOL_H

#include <QImage>
#include <QThreadPool>
#include <opencv/cv.h>

class ImageMatcher;
class ImageMatcherPool : public QThreadPool
{
    Q_OBJECT

    Q_PROPERTY(QImage image READ image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(int bestId READ bestId WRITE setBestId NOTIFY bestIdChanged)
    Q_PROPERTY(int bestPsnr READ bestPsnr WRITE setBestPsnr NOTIFY bestPsnrChanged)
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)

    QImage m_image;
    cv::Mat mat;
    int m_bestId;
    int m_bestPsnr;

    int m_remains;
    int m_count;

public:
    ImageMatcherPool();
    void imageToMat(const QImage &image, cv::OutputArray out);

    void run(const int &id);

    QImage image() const;
    int bestId() const;

    int bestPsnr() const;

    int count() const;

public slots:
    void worker_finished(int id, int result);
    void setImage(QImage image);
    void setBestId(int bestId);

    void setBestPsnr(int bestPsnr);

    void setCount(int count);

signals:
    void imageChanged(QImage image);
    void bestIdChanged(int bestId);
    void bestPsnrChanged(int bestPsnr);

    void finished(int id, int psnr);

    friend class ImageMatcher;
    void countChanged(int count);
};

#endif // IMAGEMATCHERPOOL_H
