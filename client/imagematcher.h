#ifndef IMAGEMATCHER_H
#define IMAGEMATCHER_H

#include <QRunnable>
#include <QImage>
#include <opencv/cv.h>

class ImageMatcherPool;
class ImageMatcher : public QRunnable
{
    ImageMatcherPool *_parent;
    cv::Mat _baseMat;
    int _index;

public:
    ImageMatcher(ImageMatcherPool *parent, cv::Mat baseMat, int index);

public:
    void run();
};

#endif // IMAGEMATCHER_H
