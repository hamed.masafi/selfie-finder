#include <QCoreApplication>
#include <Server>
#include <SimpleTokenValidator>
#include "../common/processor.h"
#include "../common/converter.h"
#include "../common/consts.h"
#include "imagesdata.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qRegisterMetaType<Processor*>();

    Neuron::Server *server = new Neuron::Server;
    server->setServerType(Neuron::Server::MultiThread);
    server->registerType<Processor*>();
    server->setEncoder(new Neuron::SimpleTokenValidator(NEURON_VALIDATE_TOKEN));

    ImagesData data;
    data.setCount(1000);
    data.clear();
    QObject::connect(server, &Neuron::Server::peerConnected, [&data, server](Neuron::Peer *peer){
        Processor *p = qobject_cast<Processor*>(peer);
        p->setIsBusy(false);

        QObject::connect(p, &Processor::resultReturned, [p, &data, server](int id, int psnr){
            data.setResults(id, psnr);
            if (data.isFinished()) {
                int best = data.findBest();
                int ms = data.elapsed();
                auto fn = QString(IMAGE_NAME_TEMPLATE).arg(best);
                server->forEach<Processor>([&fn, ms](Processor *p){
                    p->showImage(fn, ms);
                    p->showMessage(QString("%1 ms").arg(ms));
                    p->setIsBusy(false);
                });
            } else {
                p->setList(data.take());
                server->forEach<Processor>([&data](Processor *p){
                    p->setProgressValue(data.checked());
                });
            }
        });

        QObject::connect(p, &Processor::newImage, [&data, server](const QImage &image){
            int imageIndex = 0;
            data.clear();
            server->forEach<Processor>([&imageIndex, image, &data](Processor *p){
                p->search(image);
                p->setList(data.take());
                p->showMessage("Searching...");
                p->setProgressMax(data.count());
                p->setIsBusy(true);
                imageIndex += 10;
            });
        });
        int count = server->peers().count();
        server->forEach<Processor>([count](Processor *p){
            p->setWorkersCount(count);
        });

    });
    QObject::connect(server, &Neuron::Server::peerDisconnected, [](Neuron::Peer *peer){
        Q_UNUSED(peer);
    });

    server->startServer(PORT);

    return a.exec();
}
