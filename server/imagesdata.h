#ifndef IMAGESDATA_H
#define IMAGESDATA_H

#include <QObject>
#include <QImage>
#include <QMap>
#include <QTime>

class ImagesData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QImage image READ image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)

    QMap<int, int> data;

    QImage m_ImagesData;
    int m_count;

    QImage m_image;
    QTime time;
    int _elapsed;
    int _checked;

public:
    void clear();
    QVariantList take();
    void setResults(const QVariantMap &map);
    void setResults(int id, int psnr);
    bool isFinished() const;
    int findBest();

    int count() const;
    QImage image() const;

    int elapsed() const;

    int checked() const;

public slots:
    void setCount(int count);
    void setImage(QImage image);

signals:
    void countChanged(int count);
    void imageChanged(QImage image);
};

#endif // IMAGESDATA_H
