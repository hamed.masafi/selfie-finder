#include "imagesdata.h"

#include <QVariant>
#include <QDebug>

#define FREE -1
#define RESERVED -2

int ImagesData::elapsed() const
{
    return _elapsed;
}

int ImagesData::checked() const
{
    return _checked;
}

void ImagesData::clear()
{
    for (int i = 1; i <= m_count; ++i) {
        data.insert(i, FREE);
    }
    time.start();
    _checked = 0;
}

QVariantList ImagesData::take()
{
    int count = 0;
    QVariantList list;
    auto i = data.begin();
    while (i != data.end()) {
        if (i.value() == FREE) {
            list.append(QVariant(i.key()));
            data.insert(i.key(), RESERVED);
            if (++count == 10)
                break;
        }
        ++i;
    }
    _checked += list.count();
    return list;
}

void ImagesData::setResults(const QVariantMap &map)
{
    auto i = map.begin();
    while (i != map.end()) {
        bool ok;
        bool ok2;
        int v = i.value().toInt(&ok);
        int k = i.key().toInt(&ok2);
        if (ok && ok2)
            data.insert(k, v);
        ++i;
    }
}

void ImagesData::setResults(int id, int psnr)
{
    data.insert(id, psnr);
}

bool ImagesData::isFinished() const
{
    auto i = data.begin();
    while (i != data.end()) {
        if (i.value() == FREE)
            return false;

        ++i;
    }
    return true;
}

int ImagesData::findBest()
{
    _elapsed = time.elapsed();
    int best_key = -1;
    int best_value = -1;
    auto i = data.begin();
    while (i != data.end()) {
        if (i.key() && i.value() >= 0 && (i.value() < best_value || best_value == -1)) {
            best_key = i.key();
            best_value = i.value();
        }

        ++i;
    }
    return best_key;
}

int ImagesData::count() const
{
    return m_count;
}

QImage ImagesData::image() const
{
    return m_image;
}

void ImagesData::setCount(int count)
{
    if (m_count == count)
        return;

    m_count = count;
    emit countChanged(m_count);
}

void ImagesData::setImage(QImage image)
{
    if (m_image == image)
        return;

    m_image = image;
    emit imageChanged(m_image);
}
